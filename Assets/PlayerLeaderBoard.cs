﻿using UnityEngine;
using TMPro;

public class PlayerLeaderBoard : MonoBehaviour
{

    public TextMeshProUGUI Rank;
    public TextMeshProUGUI UserName;
    public TextMeshProUGUI Points;

    private int PlayerNo;
    private MainCanvas Mc;

    void Start()
    {
        Mc = FindObjectOfType<MainCanvas>();

        int.TryParse(gameObject.name, out PlayerNo);
    }

    void Update()
    {
        if (Mc.LeaderBoardLoaded)
        {
            int.TryParse(gameObject.name, out PlayerNo);

            Rank.text = (PlayerNo + 1).ToString();
            UserName.text = Mc.leaderBoard[PlayerNo].username.ToString();
            Points.text = Mc.leaderBoard[PlayerNo].points.ToString();
        }
    }
}
