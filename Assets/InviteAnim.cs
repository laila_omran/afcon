﻿using UnityEngine;
using DG.Tweening;
using TMPro;

public class InviteAnim : MonoBehaviour
{
    private TextMeshProUGUI Sent;

    private void Awake()
    {
        Sent = GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {
        Sent.DOFade(0, 1.5f).OnComplete(Destroyed);
    }

    void Destroyed()
    {
        Destroy(this.gameObject);
    }
}
