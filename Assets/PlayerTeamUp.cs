﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class PlayerTeamUp : MonoBehaviour
{

    public TextMeshProUGUI user_id;

    private int PlayerNo;

    private GameManager Gm;
    private TeamUpPanel Tp;
    private MainCanvas Mc;

    void Start()
    {
        Gm = FindObjectOfType<GameManager>();
        Tp = FindObjectOfType<TeamUpPanel>();
        Mc = FindObjectOfType<MainCanvas>();

        int.TryParse(gameObject.name, out PlayerNo);
    }

    void Update()
    {
        if (Mc.TeamUpLoaded)
        {
            int.TryParse(gameObject.name, out PlayerNo);

            user_id.text = Mc.teamup[PlayerNo].user_id.ToString();
        }
    }

    public void Invite()
    {
        string API = "gamesapi.playit.mobi/api/invite?from=" + Gm.UserID.ToString() + "&to=" + Mc.teamup[PlayerNo].user_id.ToString();


        StartCoroutine(GetData(API));
    }

    IEnumerator GetData(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
        }
        else
        {
            Tp.SentAnim();
        }
    }
}
