﻿using UnityEngine;

public class LeaderBoard : MonoBehaviour
{
    public GameObject PlayerTab;

    public RectTransform Panel;

    private RectTransform OwnPanel;
    private bool Spawned = false;

    private MainCanvas Mc;

    void Start()
    {
        Mc = FindObjectOfType<MainCanvas>();
        OwnPanel = GetComponent<RectTransform>();
    }

    void Update()
    {
        if (Mc.LeaderBoardLoaded)
        {
            SpawnTabs();
            OwnPanel.sizeDelta = new Vector2(300, Mc.leaderBoard.Length * 50);
        }                
    }

    void SpawnTabs()
    {
        if (!Spawned)
        {
            Spawned = true;
            for (int i = Mc.leaderBoard.Length - 1; i > -1; i--)
            {
                foreach (RectTransform child in Panel)
                {
                    child.position += Vector3.down * 50.0f;
                }

                GameObject NewPlayerTab = (GameObject)Instantiate(PlayerTab, Panel);

                NewPlayerTab.name = (i).ToString();
            }

            OwnPanel.localPosition = new Vector2(0, - (Mc.leaderBoard.Length * 50));
        }
    }
}
