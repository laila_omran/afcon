﻿using UnityEngine;


public class TeamUpPanel : MonoBehaviour
{
    public GameObject PlayerTab, Sent;

    public RectTransform Panel, SentPanel;


    private RectTransform OwnPanel;
    private bool Spawned = false;

    private MainCanvas Mc;

    void Start()
    {
        Mc = FindObjectOfType<MainCanvas>();
        OwnPanel = GetComponent<RectTransform>();
    }

    void Update()
    {
        if (Mc.TeamUpLoaded)
        {
            SpawnTabs();
            OwnPanel.sizeDelta = new Vector2(300, Mc.teamup.Length * 50);
        }
    }

    void SpawnTabs()
    {
        if (!Spawned)
        {
            Spawned = true;
            for (int i = Mc.teamup.Length - 1; i > -1; i--)
            {
                foreach (RectTransform child in Panel)
                {
                    child.position += Vector3.down * 50.0f;
                }

                GameObject NewPlayerTab = (GameObject)Instantiate(PlayerTab, Panel);

                NewPlayerTab.name = (i).ToString();
            }

            OwnPanel.localPosition = new Vector2(0, -(Mc.teamup.Length * 50));
        }
    }

    public void SentAnim()
    {
        Instantiate(Sent, SentPanel);
    }
}
