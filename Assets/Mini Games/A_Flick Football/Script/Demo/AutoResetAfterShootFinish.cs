﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoResetAfterShootFinish : MonoBehaviour
{

    private float resetAfter = 1f;
    public bool RandomNewPos { get; set; }
    // Use this for initialization


    private PlayManager Pm;

    void Awake()
    {
        Pm = FindObjectOfType<PlayManager>();
    }

        void Start ()
	{
	    RandomNewPos = true;
	    GoalDetermine.EventFinishShoot += OnShootFinished;
	    Shoot.EventDidPrepareNewTurn += OnNewTurn;
	}

    void OnDestroy()
    {
        GoalDetermine.EventFinishShoot -= OnShootFinished;
        Shoot.EventDidPrepareNewTurn -= OnNewTurn;
    }

    void OnNewTurn()
    {
        RunAfter.removeTasks(gameObject);
    }

    void OnShootFinished(bool isGoal, Area area)
    {
        if (isGoal)
        {
            Pm.RightAnswer();
            Invoke("OpenLevel", 1);
            Pm.IsGoal = false;
        }
        else
        {
            Pm.WrongAnswer();
            SceneManager.LoadScene(Pm.CurrentMap);
        }
    }

    void OpenLevel()
    {
        SceneManager.LoadScene(Pm.CurrentMap);
    }
}
