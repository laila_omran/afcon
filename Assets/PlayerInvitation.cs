﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class PlayerInvitation : MonoBehaviour
{
    public TextMeshProUGUI user_id;

    private GameManager Gm;
    private MainCanvas Mc;

    void Start()
    {
        Gm = FindObjectOfType<GameManager>();
        Mc = FindObjectOfType<MainCanvas>();
    }

    void Update()
    {
        if (Mc.InvitationLoaded)
        {
            user_id.text = Mc.invitation.id.ToString();
        }
    }

    public void Accept()
    {
        string API = "gamesapi.playit.mobi/api/updateData?user_id=" + Mc.invitation.id.ToString();


        StartCoroutine(AcceptData(API));
    }

    IEnumerator AcceptData(string url)
    {
        WWWForm form = new WWWForm();
        form.AddField("invitation_status", "true");

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                print("Invitation Accepted");

                Destroy(this.gameObject);
            }
        }
    }

    public void Reject()
    {
        string API = "gamesapi.playit.mobi/api/updateData?user_id=" + Mc.invitation.id.ToString();


        StartCoroutine(RejectData(API));
    }

    IEnumerator RejectData(string url)
    {
        WWWForm form = new WWWForm();
        form.AddField("invitation_status", "false");

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                print("Invitation Rejected");

                Destroy(this.gameObject);
            }
        }
    }
}
