﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    public string UserID = "", Country, TempCountry;
    public int CountryNo, Points, PointsAdd, Rank = 1, OpenLevel, Question = 0, MiniGame = 0, Level, LevelAdd = 0, Tutorial = 0;
    public int[] LevelsStar, LevelsLocks, LvTest;

    public int LvMapPos = 0;

    public bool IsOnline = false, DidOnlineLoad = false, DidOnlineSave = false;

    public GameObject Error;

    [Serializable]
    public class MyClass
    {
        public int level = 1;
        public int points;
        public string country;

        public int rank;
        public int map_position;
        public int question_number;
        public int country_number;
        public int tutorial;

        public int[] level_locks;
        public int[] level_stars;
    }

    public MyClass myClass;


    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameManager");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        LevelsStar = new int[101];
        LevelsLocks = new int[101];


        if (PlayerPrefs.HasKey("UserID"))
        {
            Load();
            OnlineLoad();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("t"))
        {
            MiniGame = 0;
        }
    }

    public void OpenMainMenuScene()
    {
        SceneManager.LoadScene("_Main");
    }

    public void OpenMapScene()
    {
        if (DidOnlineLoad)
        {
            SceneManager.LoadScene("_Map");
        }
    }

    public void Save()
    {
        //UserID = PlayerPrefs.GetString("UserID");
        PlayerPrefs.SetInt("Question", Question);
        PlayerPrefs.SetInt("MiniGame", MiniGame);
    }

    public void Load()
    {
        {
            UserID = PlayerPrefs.GetString("UserID");
            Question = PlayerPrefs.GetInt("Question");
            MiniGame = PlayerPrefs.GetInt("MiniGame");
        }
    }


    public void GetOnlineData()
    {
        Country = myClass.country;
        Points = myClass.points;
        Rank = myClass.rank;
        Level = myClass.level;
        LvMapPos = myClass.map_position;
        CountryNo = myClass.country_number;
        Tutorial = myClass.tutorial;

        LevelsLocks = myClass.level_locks;
        LevelsStar = myClass.level_stars;
    }

    public void OnlineLoad()
    {
        string API = "gamesapi.playit.mobi/api/getData?user_id=" + UserID;

        PlayerPrefs.SetString("UserID", UserID);

        DidOnlineLoad = false;

        StartCoroutine(GetData(API));
    }

    IEnumerator GetData(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Error.SetActive(true);
            DidOnlineLoad = false;
        }

        else
        {
            print("Online LOAD");

            myClass = JsonUtility.FromJson<MyClass>(www.downloadHandler.text);

            if (myClass.country != "null")
            {
                GetOnlineData();
            }

            DidOnlineLoad = true;
        }
    }

    public void OnlineSave()
    {
        string API = "gamesapi.playit.mobi/api/updateData?user_id=" + UserID;

        StartCoroutine(SaveData(API));
    }

    IEnumerator SaveData(string url)
    {
        WWWForm form = new WWWForm();
        form.AddField("country", Country);
        form.AddField("points", PointsAdd);
        form.AddField("level", LevelAdd);
        form.AddField("country_number", CountryNo);
        form.AddField("tutorial", Tutorial);
        form.AddField("map_position", LvMapPos);

        for (int i = 0; i < 25; i++)
        {
            form.AddField("level_locks"+"["+i.ToString()+"]", LevelsLocks[i].ToString());
        }

        for (int i = 0; i < 25; i++)
        {
            form.AddField("level_stars"+"["+i.ToString()+"]", LevelsStar[i].ToString());
        }

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                DidOnlineSave = true;

                PointsAdd = 0;
                LevelAdd = 0;

                OnlineLoad();

                print("Online SAVE");
            }
        }
    }

    public void ResetOnlineData()
    {
        string API = "gamesapi.playit.mobi/api/updateData?user_id=" + UserID;

        StartCoroutine(ResetData(API));
    }

    IEnumerator ResetData(string url)
    {
        WWWForm form = new WWWForm();
        form.AddField("country", "Choose");
        form.AddField("level", -23);
        form.AddField("country_number", 1);
        form.AddField("tutorial", 1);
        form.AddField("map_position", 1);

        for (int i = 0; i < 25; i++)
        {
            form.AddField("level_locks" + "[" + i.ToString() + "]", 0.ToString());
        }

        for (int i = 0; i < 25; i++)
        {
            form.AddField("level_stars" + "[" + i.ToString() + "]", 0.ToString());
        }

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                DidOnlineSave = true;

                PointsAdd = 0;
                LevelAdd = 0;

                OnlineLoad();

                print("Online SAVE");
            }
        }
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ResetStats()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
        Destroy(this.gameObject);
    }
}
