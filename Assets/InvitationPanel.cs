﻿using UnityEngine;
using TMPro;

public class InvitationPanel : MonoBehaviour
{
    public GameObject InvitationTab;
    public TextMeshProUGUI TeamPoints;

    private int TotalPoints = 0;

    private RectTransform Panel;
    private bool TabSpawned = false, PointsSpawned = false;

    private MainCanvas Mc;
    private GameManager Gm;

    void Start()
    {
        Mc = FindObjectOfType<MainCanvas>();
        Gm = FindObjectOfType<GameManager>();
        Panel = GetComponent<RectTransform>();
    }

    void Update()
    {
        if (Mc.InvitationLoaded)
        {
            SpawnTab();
        }

        if (Mc.MyTeamLoaded)
        {
            SpawnTotal();
        }
    }

    void SpawnTotal()
    {
        if (!PointsSpawned)
        {
            PointsSpawned = true;

            for (int i = Mc.myTeam.Length - 1; i > -1; i--)
            {
                if (Mc.myTeam[i].invitation_status == null)
                {
                    TotalPoints = TotalPoints + Mc.myTeam[i].points;
                }
            }

            TotalPoints = TotalPoints + Gm.Points;
        }

        TeamPoints.text = "Team Points = " + TotalPoints.ToString();
    }

    void SpawnTab()
    {
        if (!TabSpawned)
        {
            TabSpawned = true;
            Instantiate(InvitationTab, Panel);
        }
    }
}
