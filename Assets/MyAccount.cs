﻿using UnityEngine;
using TMPro;

public class MyAccount : MonoBehaviour
{

    public TextMeshProUGUI UserId;

    private GameManager Gm;

    void Start()
    {
        Gm = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        UserId.text = "User Id: " + Gm.UserID.ToString();
    }
}
