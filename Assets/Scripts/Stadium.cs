﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Stadium : MonoBehaviour
{
    public Sprite Unlocked, Locked, StarImage0, StarImage1, StarImage2, StarImage3;

    private GameManager Gm;
    private TextMeshProUGUI Country;
    private int LevelNo;
    private Image ButtonImage;
    private Button Button;


    void Awake()
    {
        Gm = FindObjectOfType<GameManager>();
        LevelNo = Convert.ToInt32(gameObject.name);
        ButtonImage = GetComponent<Image>();
        Button = GetComponent<Button>();
        Country = GetComponentInChildren<TextMeshProUGUI>();
    }

    void Update()
    {
        if (Gm.Country == Country.text)
        {
            this.gameObject.SetActive(false);
        }

        if (Gm.LevelsLocks[LevelNo] == 0)
        {
            ButtonImage.sprite = Locked;
            Button.enabled = false;
        }

        else if (Gm.LevelsLocks[LevelNo] == 1)
        {
            ButtonImage.sprite = Unlocked;
            Button.enabled = true;
        }

        else if (Gm.LevelsLocks[LevelNo] == 2)
        {
            switch (Gm.LevelsStar[LevelNo])
            {
                case 0:
                    ButtonImage.sprite = StarImage0;
                    break;
                case 1:
                    ButtonImage.sprite = StarImage1;
                    break;
                case 2:
                    ButtonImage.sprite = StarImage2;
                    break;
                case 3:
                    ButtonImage.sprite = StarImage3;
                    break;
            }
            Button.enabled = false;
        }
    }

    public void OpenLevel()
    {
        if (Gm.LevelsLocks[LevelNo] == 1)
        {
            Gm.Save();
            Gm.OnlineSave();
            SceneManager.LoadScene(LevelNo);
        }
    }
}
