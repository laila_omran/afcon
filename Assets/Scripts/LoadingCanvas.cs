﻿using UnityEngine;

public class LoadingCanvas : MonoBehaviour
{
    public GameObject LoadingPanel;

    private GameManager Gm;

    private void Awake()
    {
        Gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        if (!Gm.DidOnlineLoad)
        {
            LoadingPanel.SetActive(true);
        }
        else
        {
            LoadingPanel.SetActive(false);
        }
    }
}
