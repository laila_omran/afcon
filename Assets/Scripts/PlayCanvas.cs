﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayCanvas : MonoBehaviour
{
    public Image PlayerCountryImage, MiniGameImg, TriviaImg;
    public Button MiniGameBtn, TriviaBtn;
    public TextMeshProUGUI Time, PlayerScore, OpponentScore, PlayerMode, OpponentMode;
    public GameObject ChallengePanel, TriviaPanel;
    public int CurrentPos;

    public GameObject[] Questions;

    private Sprite PlayerFlag;

    private Canvas MainCanvas;

    private GameManager Gm;
    private PlayManager Pm;

    private int QuestionNo;

    private void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("PlayCanvas");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        MainCanvas = GetComponent<Canvas>();
    }

    void Start()
    {
        Gm = FindObjectOfType<GameManager>();
        Pm = FindObjectOfType<PlayManager>();

        PlayerFlag = Resources.Load<Sprite>("Flags/" + Gm.Country);
        PlayerCountryImage.sprite = PlayerFlag;
    }

    void Update()
    {
        Time.text = Pm.Minutes.ToString("00") + ":" + Pm.Seconds.ToString("00");
        PlayerScore.text = Pm.PlayerScore.ToString();
        OpponentScore.text = Pm.OpponentScore.ToString();

        if (Pm.CurrentMap != SceneManager.GetActiveScene().buildIndex)
        {
            MainCanvas.enabled = false;
        }

        else
        {
            MainCanvas.enabled = true;
        }

        if (Pm.Attacking)
        {
            PlayerMode.text = "Attacking";
            OpponentMode.text = "Defending";
        }
        else
        {
            PlayerMode.text = "Defending";
            OpponentMode.text = "Attacking";
        }

        if (Pm.HasLost)
        {
            HidePanels();
        }

        if (Gm.Question == 2)
        {
            TriviaImg.color = new Color(1, 1, 1, .25f);
            TriviaBtn.enabled = false;
        }
        else
        {
            TriviaImg.color = new Color(1, 1, 1, 1);
            TriviaBtn.enabled = true;
        }

        if (Gm.MiniGame == 2)
        {
            MiniGameImg.color = new Color(1, 1, 1, .25f);
            MiniGameBtn.enabled = false;
        }
        else
        {
            MiniGameImg.color = new Color(1, 1, 1, 1);
            MiniGameBtn.enabled = true;
        }
    }

    public void ShowChallengePanel()
    {
        ChallengePanel.SetActive(true);
        TriviaPanel.SetActive(false);

        Gm.Load();
    }

    public void MiniGameButton()
    {
        Pm.OpenPenaltyMap();
        Gm.MiniGame++;
        Gm.Question = 0;
        Gm.Save();
        HidePanels();
    }

    public void ShowTriviaPanel()
    {
        TriviaPanel.SetActive(true);

        QuestionNo = Random.Range(0, 51);

        Instantiate(Questions[QuestionNo], TriviaPanel.transform);

        Gm.Question++;
        Gm.MiniGame = 0;
        Gm.Save();

        Gm.OnlineSave();
        ChallengePanel.SetActive(false); 
    }

    public void HidePanels()
    {
        TriviaPanel.SetActive(false);
        ChallengePanel.SetActive(false);
    }

    public void DestroyPlayCanvas()
    {
        Destroy(this.gameObject);
    }
}