﻿using UnityEngine;
using DG.Tweening;

public class StartPanel : MonoBehaviour
{
    public RectTransform Info1, Info2, Info3, Info4;

    private GameManager Gm;
    private PlayManager Pm;

    void Start()
    {
        Gm = FindObjectOfType<GameManager>();
        Pm = FindObjectOfType<PlayManager>();

        Next1();

        if (Gm.Tutorial == 2)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void Next1()
    {
        Info1.DOAnchorPosX(0, 0.25f);
        Info2.DOAnchorPosX(400, 0.25f);
        Info3.DOAnchorPosX(400, 0.25f);
        Info4.DOAnchorPosX(400, 0.25f);
    }

    public void Next2()
    {
        Info1.DOAnchorPosX(-400, 0.25f);
        Info2.DOAnchorPosX(0, 0.25f);
        Info3.DOAnchorPosX(400, 0.25f);
        Info4.DOAnchorPosX(400, 0.25f);
    }

    public void Next3()
    {
        Info1.DOAnchorPosX(-400, 0.25f);
        Info2.DOAnchorPosX(-400, 0.25f);
        Info3.DOAnchorPosX(0, 0.25f);
        Info4.DOAnchorPosX(400, 0.25f);
    }

    public void Next4()
    {
        Info1.DOAnchorPosX(-400, 0.25f);
        Info2.DOAnchorPosX(-400, 0.25f);
        Info3.DOAnchorPosX(-400, 0.25f);
        Info4.DOAnchorPosX(0, 0.25f);
    }

    public void Next5()
    {
        Gm.Tutorial = 2;
        Gm.Save();
        Gm.OnlineSave();
        Pm.HasStarted = true;
        this.gameObject.SetActive(false);
    }

}
