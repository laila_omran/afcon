﻿using UnityEngine;
using DG.Tweening;
using TMPro;

public class WinPanel : MonoBehaviour
{
    public TextMeshProUGUI WinText;
    public RectTransform Star1, Star2, Star3;

    private GameManager Gm;
    private PlayManager Pm;

    void Start()
    {
        Gm = FindObjectOfType<GameManager>();
        Pm = FindObjectOfType<PlayManager>();

        switch (Pm.Stars)
        {
            case 1:
                Star1.DOScale(1, 1);
                break;
            case 2:
                Star1.DOScale(1, 1);
                Star2.DOScale(1, 1);
                break;
            case 3:
                Star1.DOScale(1, 1);
                Star2.DOScale(1, 1);
                Star3.DOScale(1, 1);
                break;
        }
    }

    void Update()
    {
        WinText.text = "\n\n\nYou earned Points = " + Pm.Points.ToString("0");
    }
}