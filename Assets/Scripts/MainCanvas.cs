﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using DG.Tweening;

public class MainCanvas : MonoBehaviour
{
    public RectTransform Menu, TeamChoose, Flags, Leaderboard, MyAccount, Teamup, _Invitation;
    public GameObject InternetPanel, SignInPanel;

    public bool LeaderBoardLoaded = false, TeamUpLoaded = false, InvitationLoaded = false, MyTeamLoaded = false;

    public Text EnteredUserID;

    private GameManager Gm;


    private void Awake()
    {
        Gm = FindObjectOfType<GameManager>();
    }

    private void Start()
    {
        if (Gm.UserID == "")
        {
            SignInPanel.SetActive(true);
        }
        else
        {
            SignInPanel.SetActive(false);
        }

        TeamUpLoadLoad();
        MyTeamLoad();
        Invitations();
        LeaderboardLoad();
    }

    private void Update()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            InternetPanel.SetActive(false);
            Gm.IsOnline = true;
        }
        else
        {
            InternetPanel.SetActive(true);
            Gm.IsOnline = false;
        }
    }

    public void PlayButton()
    {
        if (Gm.Country == "" || Gm.Country == "Choose")
        {
            TeamChoosePanel();
        }

        else
        {
            Gm.OpenMapScene();
        }
    }

    public void MenuPanel()
    {
        Menu.DOAnchorPos(new Vector2(0, 0), 0.25f);
        TeamChoose.DOAnchorPos(new Vector2(400, 0), 0.25f);
        Flags.DOAnchorPos(new Vector2(2700, 0), 0.25f);
        Leaderboard.DOAnchorPos(new Vector2(-400, 0), 0.25f);
        MyAccount.DOAnchorPos(new Vector2(0, -700), 0.25f);
        Teamup.DOAnchorPos(new Vector2(0, 700), 0.25f);
        _Invitation.DOAnchorPos(new Vector2(400, 700), 0.25f);
    }

    public void TeamChoosePanel()
    {
        Menu.DOAnchorPos(new Vector2(-400, 0), 0.25f);
        TeamChoose.DOAnchorPos(new Vector2(0, 0), 0.25f);
        Leaderboard.DOAnchorPos(new Vector2(-400, 0), 0.25f);
        MyAccount.DOAnchorPos(new Vector2(0, -700), 0.25f);
        Teamup.DOAnchorPos(new Vector2(0, 700), 0.25f);
        _Invitation.DOAnchorPos(new Vector2(400, 700), 0.25f);
    }

    public void LeaderboardPanel()
    {
        Menu.DOAnchorPos(new Vector2(400, 0), 0.25f);
        TeamChoose.DOAnchorPos(new Vector2(400, 0), 0.25f);
        Leaderboard.DOAnchorPos(new Vector2(0, 0), 0.25f);
        MyAccount.DOAnchorPos(new Vector2(0, -700), 0.25f);
        Teamup.DOAnchorPos(new Vector2(0, 700), 0.25f);
        _Invitation.DOAnchorPos(new Vector2(400, 700), 0.25f);
    }

    public void MyAccountPanel()
    {
        Menu.DOAnchorPos(new Vector2(0, 700), 0.25f);
        TeamChoose.DOAnchorPos(new Vector2(400, 700), 0.25f);
        Leaderboard.DOAnchorPos(new Vector2(-400, 700), 0.25f);
        MyAccount.DOAnchorPos(new Vector2(0, 0), 0.25f);
        Teamup.DOAnchorPos(new Vector2(0, 1400), 0.25f);
        _Invitation.DOAnchorPos(new Vector2(400, 1400), 0.25f);
    }

    public void TeamUpPanel()
    {
        Menu.DOAnchorPos(new Vector2(0, -700), 0.25f);
        TeamChoose.DOAnchorPos(new Vector2(400, -700), 0.25f);
        Leaderboard.DOAnchorPos(new Vector2(-400, -700), 0.25f);
        MyAccount.DOAnchorPos(new Vector2(0, -1400), 0.25f);
        Teamup.DOAnchorPos(new Vector2(0, 0), 0.25f);
        _Invitation.DOAnchorPos(new Vector2(400, 0), 0.25f);
    }

    public void InvitationPanel()
    {
        Menu.DOAnchorPos(new Vector2(0, -700), 0.25f);
        TeamChoose.DOAnchorPos(new Vector2(400, -700), 0.25f);
        Leaderboard.DOAnchorPos(new Vector2(-400, -700), 0.25f);
        MyAccount.DOAnchorPos(new Vector2(0, -1400), 0.25f);
        Teamup.DOAnchorPos(new Vector2(-400, 0), 0.25f);
        _Invitation.DOAnchorPos(new Vector2(0, 0), 0.25f);
    }


    public void RegisterNumber()
    {
        Gm.UserID = EnteredUserID.text;
        Gm.OnlineLoad();

        SignInPanel.SetActive(false);
    }

    public void Reset()
    {
        Gm.ResetStats();
    }


    [Serializable]
    public class LeaderBoard
    {
        public int points;
        public string username;
    }

    public LeaderBoard[] leaderBoard;

    public void LeaderboardLoad()
    {
        string API = "gamesapi.playit.mobi/api/getLeaderBoard";


        StartCoroutine(GetLeaderboardData(API));
    }

    IEnumerator GetLeaderboardData(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Gm.Error.SetActive(true);
        }

        else
        {
            print("Leaderboard LOAD");

            leaderBoard = UnityRest.JsonHelper.FromJson<LeaderBoard>(www.downloadHandler.text);

            LeaderBoardLoaded = true;
        }
    }

    [Serializable]
    public class TeamUp
    {
        public string username;
        public string user_id;
    }

    public TeamUp[] teamup;

    public void TeamUpLoadLoad()
    {
        string API = "gamesapi.playit.mobi/api/getUsersToInvite?user_id=" + Gm.UserID.ToString();


        StartCoroutine(GetTeamUpData(API));
    }

    IEnumerator GetTeamUpData(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Gm.Error.SetActive(true);
        }

        else
        {
            print("TeamUp LOAD");

            teamup = UnityRest.JsonHelper.FromJson<TeamUp>(www.downloadHandler.text);

            TeamUpLoaded = true;
        }
    }

    [Serializable]
    public class MyTeam
    {
        public int points;
        public string invitation_status;
        public string user_id;
    }

    public MyTeam[] myTeam;

    public void MyTeamLoad()
    {
        string API = "gamesapi.playit.mobi/api/getTeam?user_id=" + Gm.UserID.ToString();


        StartCoroutine(GetMyTeamData(API));
    }

    IEnumerator GetMyTeamData(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Gm.Error.SetActive(true);
        }

        else
        {
            print("MyTeam LOAD");

            myTeam = UnityRest.JsonHelper.FromJson<MyTeam>(www.downloadHandler.text);

            MyTeamLoaded = true;
        }
    }

    public void Invitations()
    {
        string API = "gamesapi.playit.mobi/api/getInvitation?user_id=" + Gm.UserID;

        StartCoroutine(GetInvitations(API));
    }

    IEnumerator GetInvitations(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            print("Error");
        }

        else
        {
            print("Invitations LOAD");

            invitation = JsonUtility.FromJson<Invitation>(www.downloadHandler.text);
            InvitationLoaded = true;
        }
    }

    [Serializable]
    public class Invitation
    {
        public string username;
        public string id;
    }

    public Invitation invitation;
}


namespace UnityRest
{
    public static class JsonHelper
    {
        public static T[] FromJson<T>(string jsonArray)
        {
            jsonArray = WrapArray(jsonArray);
            return FromJsonWrapped<T>(jsonArray);
        }

        public static T[] FromJsonWrapped<T>(string jsonObject)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(jsonObject);
            return wrapper.items;
        }

        private static string WrapArray(string jsonArray)
        {
            return "{ \"items\": " + jsonArray + "}";
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] items;
        }
    }
}
