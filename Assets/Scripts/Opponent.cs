﻿using UnityEngine;
using UnityEngine.UI;

public class Opponent : MonoBehaviour
{
    private PlayManager Pm;
    private Button Button;

    void Start()
    {
        Pm = FindObjectOfType<PlayManager>();
        Button = GetComponent<Button>();
    }

    void Update()
    {
        if (!Pm.Attacking)
        {
            Button.enabled = true;
        }
        else
        {
            Button.enabled = false;
        }
    }
}
