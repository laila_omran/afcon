﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseTrigger : MonoBehaviour
{
    private PlayManager Pm;
    private bool Wait = false;

    private void Start()
    {
        Pm = FindObjectOfType<PlayManager>();
    }

    void OnTriggerEnter(Collider target)
    {
        if (target.gameObject.tag == "Ball")
        {
            if (!Wait)
            {
                Wait = true;
                Pm.WrongAnswer();
                Invoke("OpenScene", 1);
            }
        }
    }

    void OpenScene()
    {
        SceneManager.LoadScene(Pm.CurrentMap);
    }
}
