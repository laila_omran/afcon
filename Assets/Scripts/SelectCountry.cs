﻿using UnityEngine;
using TMPro;

public class SelectCountry : MonoBehaviour
{
    private GameManager Gm;

    public int CountryNo;
    public TextMeshProUGUI ConfirmText;
    public GameObject ConfirmationPanel;

    private void Start()
    {
        Gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        ConfirmText.text = "You chose " + Gm.TempCountry.ToString() + ", Once you choose a team you can't change it, Continue?";
    }

    public void ShowConfirmation()
    {
        ConfirmationPanel.SetActive(true);
    }

    public void HideConfirmation()
    {
        ConfirmationPanel.SetActive(false);
    }

    public void CountryConfirm()
    {
        Gm.Country = Gm.TempCountry;
        Gm.OnlineSave();
        Gm.Save();
        Gm.OpenMapScene();
    }

    public void CountrySelect()
    {
        Gm.TempCountry = gameObject.name;
        Gm.CountryNo = CountryNo;

        if (CountryNo == 1)
        {
            Gm.LevelsLocks[2] = 1;
            Gm.LvMapPos = 2;
        }
        else
        {
            Gm.LevelsLocks[1] = 1;
            Gm.LvMapPos = 1;
        }

        ConfirmationPanel.SetActive(true);
    }
}
