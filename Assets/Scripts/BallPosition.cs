﻿using UnityEngine;
using UnityEngine.UI;

public class BallPosition : MonoBehaviour
{
    public int CurrentPos;
    public Image Lock;

    private PlayManager Pm;
    private PlayCanvas PlayCanvas;
    private Button Button;
    private Image Img;

    void Start()
    {
        Pm = FindObjectOfType<PlayManager>();
        PlayCanvas = FindObjectOfType<PlayCanvas>();
        Button = GetComponent<Button>();
        Img = GetComponent<Image>();
    }

    void Update()
    {
        if (Pm.Attacking)
        {
            if (Pm.BallPosition == CurrentPos)
            {
                Img.color = new Color(.5f, .5f, .5f);
                Lock.enabled = false;
                Button.enabled = true;
            }

            else
            {
                Img.color = new Color(1, 1, 1);
                Button.enabled = false;
            }
        }
        else
        {
            Button.enabled = false;
            Img.color = new Color(1, 1, 1);
        }
    }

    public void SetCurrentPos()
    {
        PlayCanvas.CurrentPos = CurrentPos;
    }
}
