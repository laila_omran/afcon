﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class PlayManager : MonoBehaviour
{
    public GameObject one, two, three, four, five, six, seven, eight, nine, ball, goal, opponentOne, opponentTwo, opponentThree, opponentFour, opponentFive, opponentSix, opponentSeven, opponentEight, opponentNine, owngoal;
    public GameObject WinPanel, LostPanel;

    public int BallPosition = 0;
    public int BallPositionOpponent = 1;
    public int PlayerToShoot;
    public int PlayerScore, OpponentScore;
    public int Minutes;
    public float Seconds;

    public bool Attacking = true;

    public bool HasWon = false;
    public bool HasLost = false;
    public bool HasStarted = false;
    public bool Won = false;
    public bool Lost = false;
    public bool RightAnswerWait = false;
    public bool WrongAnswerWait = false;
    public bool StopTimer = false;

    public bool IsGoal = false;

    public int Points, Stars, CurrentMap;

    public AudioSource ButtonSFX;
    public AudioSource GoalSFX;

    private GameManager Gm;

    private void Awake()
    {
        Gm = FindObjectOfType<GameManager>();

        GameObject[] objs = GameObject.FindGameObjectsWithTag("PlayManager");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        MoveBall();

        CurrentMap = SceneManager.GetActiveScene().buildIndex;

        if (Gm.Tutorial == 2)
        {
            HasStarted = true;
        }
    }

    private void Update()
    {
        if (HasWon)
        {
            WinWait();
            HasLost = false;
        }

        if (HasLost && !HasWon)
        {
            LoseWait();
        }

        if (HasStarted)
        {
            if (Minutes >= 90)
            {
                HasLost = true;
                StopTimer = true;
            }

            if (!StopTimer)
            {
                if (Seconds >= 59)
                {
                    Seconds = 0;
                    Minutes = Minutes + 1;
                }
                else
                {
                    Seconds = Seconds + (9 * Time.deltaTime);
                }
            }
        }
    }

    public void RightAnswer()
    {
        if (!RightAnswerWait)
        {
            if (Attacking)
            {
                if (BallPosition != PlayerToShoot)
                {
                    if (SceneManager.GetActiveScene().buildIndex != CurrentMap)
                    {
                        Invoke("MoveBall", 1);
                    }

                    else
                    {
                        MoveBall();
                    }
                }
                else 
                {
                    if (SceneManager.GetActiveScene().buildIndex != CurrentMap)
                    {
                        Invoke("ScoreGoal", 1);
                    }

                    else
                    {
                        ScoreGoal();
                    }
                }
            }
            else
            {
                BallPosition = BallPositionOpponent;

                if (SceneManager.GetActiveScene().buildIndex != CurrentMap)
                {
                    Invoke("MoveBall", 1);
                }

                else
                {
                    MoveBall();
                }

                Attacking = true;
            }
        }
        RightAnswerWait = true;
        Invoke("RightAnswerWaitFun", 2);
    }

    public void OpenPenaltyMap()
    {
        SceneManager.LoadScene("2.ShootWall");
    }

    public void WrongAnswer()
    {
        if (!WrongAnswerWait)
        {
            if (!RightAnswerWait)
            {
                BallPosition = BallPositionOpponent;

                DefendBall();

                Attacking = false;
            }
        }
        WrongAnswerWait = true;
        Invoke("WrongAnswerWaitFun", 2);
    }

    public void DefendBall()
    {
        switch (BallPositionOpponent)
        {
            case 0:
                ball.transform.DOMove(new Vector3(owngoal.transform.position.x, owngoal.transform.position.y), 1);
                OpponentScore = 1;
                HasLost = true;
                StopTimer = true;
                break;
            case 1:
                ball.transform.DOMove(new Vector3(opponentOne.transform.position.x, opponentOne.transform.position.y - 30), 1);
                break;
            case 2:
                ball.transform.DOMove(new Vector3(opponentTwo.transform.position.x, opponentTwo.transform.position.y - 30), 1);
                break;
            case 3:
                ball.transform.DOMove(new Vector3(opponentThree.transform.position.x, opponentThree.transform.position.y - 30), 1);
                break;
            case 4:
                ball.transform.DOMove(new Vector3(opponentFour.transform.position.x, opponentFour.transform.position.y - 30), 1);
                break;
            case 5:
                ball.transform.DOMove(new Vector3(opponentFive.transform.position.x, opponentFive.transform.position.y - 30), 1);
                break;
            case 6:
                ball.transform.DOMove(new Vector3(opponentSix.transform.position.x, opponentSix.transform.position.y - 30), 1);
                break;
            case 7:
                ball.transform.DOMove(new Vector3(opponentSeven.transform.position.x, opponentSeven.transform.position.y - 30), 1);
                break;
            case 8:
                ball.transform.DOMove(new Vector3(opponentEight.transform.position.x, opponentEight.transform.position.y - 30), 1);
                break;
            case 9:
                ball.transform.DOMove(new Vector3(opponentNine.transform.position.x, opponentNine.transform.position.y - 30), 1);
                break;
        }
        BallPositionOpponent--;
    }

    public void MoveBall()
    {
        switch (BallPosition)
        {
            case 0:
                ball.transform.DOMove((new Vector3(one.transform.position.x, one.transform.position.y + 30)), .1f);
                break;
            case 1:
                ball.transform.DOMove(new Vector3(two.transform.position.x, two.transform.position.y + 30), 1);
                break;
            case 2:
                ball.transform.DOMove(new Vector3(three.transform.position.x, three.transform.position.y + 30), 1);
                break;
            case 3:
                ball.transform.DOMove(new Vector3(four.transform.position.x, four.transform.position.y + 30), 1);
                break;
            case 4:
                ball.transform.DOMove(new Vector3(five.transform.position.x, five.transform.position.y + 30), 1);
                break;
            case 5:
                ball.transform.DOMove(new Vector3(six.transform.position.x, six.transform.position.y + 30), 1);
                break;
            case 6:
                ball.transform.DOMove(new Vector3(seven.transform.position.x, seven.transform.position.y + 30), 1);
                break;
            case 7:
                ball.transform.DOMove(new Vector3(eight.transform.position.x, eight.transform.position.y + 30), 1);
                break;
            case 8:
                ball.transform.DOMove(new Vector3(nine.transform.position.x, nine.transform.position.y + 30), 1);
                break;
        }
        BallPosition++;
        BallPositionOpponent = BallPosition;
    }

    void ScoreGoal()
    {
        ball.transform.DOMove(new Vector3(goal.transform.position.x, goal.transform.position.y), 1);
        PlayerScore = 1;
        StopTimer = true;
        HasWon = true;
    }

    void WinWait()
    {
        bool Wait = false;
        if (!Wait)
        {
            Wait = true;
            Invoke("WinFunction", 2);
        }
    }

    public void WinFunction()
    {
        if (!Won)
        {
            if (SceneManager.GetActiveScene().buildIndex != CurrentMap)
            {
                SceneManager.LoadScene(CurrentMap);
            }

            Won = true;
            Points = 100 - Minutes;

            if (Minutes <= 30)
            {
                Stars = 3;
            }

            else if (Minutes > 30 && Minutes <= 60)
            {
                Stars = 2;
            }

            else if (Minutes > 60)
            {
                Stars = 1;
            }

            Gm.PointsAdd = Points;
            Gm.LevelAdd = 1;
            Gm.LevelsStar[CurrentMap] = Stars;
            Gm.LevelsLocks[CurrentMap] = 2;

            if (CurrentMap + 1 == Gm.CountryNo)
            {
                Gm.LevelsLocks[CurrentMap + 2] = 1;
            }

            else
            {
                Gm.LevelsLocks[CurrentMap + 1] = 1;
            }

            StopTimer = true;
            Gm.LvMapPos = CurrentMap + 1;
            Gm.Save();
            Gm.OnlineSave();
            WinPanel.SetActive(true);
        }
    }

    void LoseWait()
    {
        bool Wait = false;
        if (!Wait && !HasWon)
        {
            Wait = true;
            Invoke("LostFunction", 2);
        }
    }

    public void LostFunction()
    {
        if (!Lost && !HasWon)
        {
            if (SceneManager.GetActiveScene().buildIndex != CurrentMap)
            {
                SceneManager.LoadScene(CurrentMap);
            }

            Lost = true;
            StopTimer = true;
            LostPanel.SetActive(true);
        }
    }

    public void OpenMapScene()
    {
        SceneManager.LoadScene("_Map");
    }

    void RightAnswerWaitFun()
    {
        RightAnswerWait = false;
    }

    void WrongAnswerWaitFun()
    {
        WrongAnswerWait = false;
    }

    public void PlayButtonSFX()
    {
        ButtonSFX.Play();
    }

    public void PlayGoalSFX()
    {
        GoalSFX.Play();
    }

    public void DestroyPlayManager()
    {
        Destroy(this.gameObject);
    }
}