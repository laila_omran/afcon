﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class MapCanvas : MonoBehaviour
{
    public Image CountryImage;
    public TextMeshProUGUI CountryText;
    public TextMeshProUGUI PointsText;
    public TextMeshProUGUI LevelText;
    public RectTransform MapPos;

    public GameObject CompletedPanel;

    private Sprite Flag;
    private int LvMapPos;
    private Vector2 LvMapVec;

    private GameManager Gm;

    private void Awake()
    {
        Gm = FindObjectOfType<GameManager>();

        LvMapPos = Gm.LvMapPos;
    }

    private void Start()
    {
        switch (LvMapPos)
        {
            case 1:
                LvMapVec = new Vector2(-200, 875);
                break;
            case 2:
                LvMapVec = new Vector2(-825, 680);
                break;
            case 3:
                LvMapVec = new Vector2(30, 745);
                break;
            case 4:
                LvMapVec = new Vector2(-330, 640);
                break;
            case 5:
                LvMapVec = new Vector2(45, 455);
                break;
            case 6:
                LvMapVec = new Vector2(-500, 240);
                break;
            case 7:
                LvMapVec = new Vector2(-585, 35);
                break;
            case 8:
                LvMapVec = new Vector2(-435, -5);
                break;
            case 9:
                LvMapVec = new Vector2(-350, 150);
                break;
            case 10:
                LvMapVec = new Vector2(-115, 120);
                break;
            case 11:
                LvMapVec = new Vector2(165, -65);
                break;
            case 12:
                LvMapVec = new Vector2(310, -225);
                break;
            case 13:
                LvMapVec = new Vector2(465, -190);
                break;
            case 14:
                LvMapVec = new Vector2(585, -150);
                break;
            case 15:
                LvMapVec = new Vector2(715, -190);
                break;
            case 16:
                LvMapVec = new Vector2(835, -295);
                break;
            case 17:
                LvMapVec = new Vector2(955, -325);
                break;
            case 18:
                LvMapVec = new Vector2(955, -480);
                break;
            case 19:
                LvMapVec = new Vector2(615, -525);
                break;
            case 20:
                LvMapVec = new Vector2(795, -650);
                break;
            case 21:
                LvMapVec = new Vector2(665, -875);
                break;
            case 22:
                LvMapVec = new Vector2(410, -820);
                break;
            case 23:
                LvMapVec = new Vector2(210, -875);
                break;
            case 24:
                LvMapVec = new Vector2(300, -770);
                break;
        }

        MapPos.DOScale(1, 3);
        MapPos.DOLocalMove(LvMapVec, 3);
    }

    private void Update()
    {
        CountryText.text = "You Are\n\n\n\n" + Gm.Country;
        PointsText.text = "You Have\n" + Gm.Points + "\n Points";
        LevelText.text = "Level\n" + Gm.Level;

        Flag = Resources.Load<Sprite>("Flags/" + Gm.Country);
        CountryImage.sprite = Flag;

        if (Gm.Level == 24)
        {
            CompletedPanel.SetActive(true);
        }
        else
        {
            CompletedPanel.SetActive(false);
        }
    }

    public void OpenMainScene()
    {
        Gm.OpenMainMenuScene();
    }

    public void ResetStats()
    {
        Gm.ResetOnlineData();
        Gm.OpenMainMenuScene();
    }
}
