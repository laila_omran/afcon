﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class TimeCanvas : MonoBehaviour
{
    public TextMeshProUGUI MainTime;
    public TextMeshProUGUI Timer;

    private PlayManager Pm;
    private PlayCanvas PlayCanvas;
    private float Seconds = 30;

    private bool Wait = false;

    void Start()
    {
        Pm = FindObjectOfType<PlayManager>();
        PlayCanvas = FindObjectOfType<PlayCanvas>();
    }

    void Update()
    {
        MainTime.text = Pm.Minutes.ToString("00") + ":" + Pm.Seconds.ToString("00");

        if (Seconds > 0)
        {
            Seconds = Seconds - (1 * Time.deltaTime);
        }
        else
        {
            if (!Wait)
            {
                Wait = true;
                Pm.WrongAnswer();
                PlayCanvas.HidePanels();
                Invoke("OpenScene", 1);
            }
        }

        Timer.text = Seconds.ToString("00");
    }

    void OpenScene()
    {
        SceneManager.LoadScene(Pm.CurrentMap);
    }
}