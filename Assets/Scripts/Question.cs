﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Question : MonoBehaviour
{
    public TextMeshProUGUI TimerText;

    private PlayCanvas PlayCanvas;
    private PlayManager Pm;
    private Image Image;

    private float Timer = 30;

    private void Start()
    {
        PlayCanvas = FindObjectOfType<PlayCanvas>();
        Pm = FindObjectOfType<PlayManager>();

        Image = GetComponent<Image>();
    }

    private void Update()
    {
        Timer = Timer - Time.deltaTime;

        TimerText.text = Timer.ToString("0");

        if (Timer <= 0)
        {
            Pm.WrongAnswer();
            Hide();
            Destroy(this.gameObject);
        }

        if (Pm.HasLost)
        {
            Destroy(this.gameObject);
        }
    }

    public void RightAnswer()
    {
        Image.color = new Color(0, 1, 0, 1);
        Invoke("Right", .5f);
    }

    public void WrongAnswer()
    {
        Image.color = new Color(1, 0, 0, 1);
        Invoke("Wrong", .5f);
    }

    void Right()
    {
        Pm.RightAnswer();
        Pm.PlayGoalSFX();
        Pm.PlayButtonSFX();
        Hide();
        Destroy(this.gameObject);
    }

    void Wrong()
    {
        Pm.WrongAnswer();
        Pm.PlayButtonSFX();
        Hide();
        Destroy(this.gameObject);
    }

    void Hide()
    {
        PlayCanvas.HidePanels();
    }
}
