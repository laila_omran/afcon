﻿using UnityEngine;
using TMPro;

public class PlayerRank : MonoBehaviour
{
    public TextMeshProUGUI Points;
    public TextMeshProUGUI Rank;

    private GameManager Gm;

    void Start()
    {
        Gm = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        Points.text = Gm.Points.ToString();
        Rank.text = Gm.Rank.ToString();
    }
}
